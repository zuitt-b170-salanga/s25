/* s25 discussion.js */


// 4docs  apple mango avocado strawberry

db.fruits.insertMany([
    {
        name: "apple",
        color: "red",
        stock: 16,
        price: 20,
        supplier_id: "111aaa",
        onSale: true,
        origin: ["China", "U.S."]
    },
    {
        name: "mango",
        color: "yellow",
        stock: 46,
        price: 10,
        supplier_id: "222bbb",
        onSale: true,
        origin: ["Philippines", "Thailand"]
    },
    {
        name: "avocado",
        color: "green",
        stock: 57,
        price: 50,
        supplier_id: "333ccc",
        onSale: true,
        origin: ["Mexico", "Indonesia"]
    },
    {
        name: "strawberry",
        color: "red",
        stock: 1000,
        price: 100,
        supplier_id: "444ddd",
        onSale: true,
        origin: ["Philippines", "Japan"]
    }
   ]);

//===========================
/* 1 */
{
    "acknowledged" : true,
    "insertedIds" : [ 
        ObjectId("62555a1d25b252e266ad211a"), 
        ObjectId("62555a1d25b252e266ad211b"), 
        ObjectId("62555a1d25b252e266ad211c"), 
        ObjectId("62555a1d25b252e266ad211d")
    ]
}
//===========================

/* 1 */
{
    "_id" : ObjectId("62555a1c25b252e266ad2112"),
    "name" : "apple",
    "color" : "red",
    "stock" : 16.0,
    "price" : 20.0,
    "supplier_id" : "111aaa",
    "onSale" : true,
    "origin" : [ 
        "China", 
        "U.S."
    ]
}

/* 2 */
{
    "_id" : ObjectId("62555a1c25b252e266ad2113"),
    "name" : "mango",
    "color" : "yellow",
    "stock" : 46.0,
    "price" : 10.0,
    "supplier_id" : "222bbb",
    "onSale" : true,
    "origin" : [ 
        "Philippines", 
        "Thailand"
    ]
}

/* 3 */
{
    "_id" : ObjectId("62555a1c25b252e266ad2114"),
    "name" : "avocado",
    "color" : "green",
    "stock" : 57.0,
    "price" : 50.0,
    "supplier_id" : "333ccc",
    "onSale" : true,
    "origin" : [ 
        "Mexico", 
        "Indonesia"
    ]
}

/* 4 */
{
    "_id" : ObjectId("62555a1c25b252e266ad2115"),
    "name" : "strawberry",
    "color" : "red",
    "stock" : 1000.0,
    "price" : 100.0,
    "supplier_id" : "444ddd",
    "onSale" : true,
    "origin" : [ 
        "Philippines", 
        "Japan"
    ]
}
//========================

db.fruits.aggregate([
    {$match: {onSale: true}}
])

//=========================

activity:
update your documents and make only two supplier id : supplier id 1 and 2
//===========================
db.fruits.updateOne(
    {
        supplier_id: "111aaa"
    },
    {
        $set: {
            supplier_id : "1"
        }
    }
)
// **************************************
db.fruits.updateOne(
    {
        supplier_id: "222bbb"
    },
    {
        $set: {
            supplier_id : "1"
        }
    }
)

// **************************************
db.fruits.updateOne(
    {
        supplier_id: "333ccc"
    },
    {
        $set: {
            supplier_id : "2"
        }
    }
)


// **************************************
db.fruits.updateOne(
    {
        supplier_id: "444ddd"
    },
    {
        $set: {
            supplier_id : "2"
        }
    }
)
// **************************************

db.fruits.updateOne(
    {
        name: "strawberry"
    },
    {
        $set: {
            onSale : false
        }
    }
)


// **************************************

// ============ output ==============
/* 1 */
{
    "_id" : ObjectId("62555a1c25b252e266ad2112"),
    "name" : "apple",
    "color" : "red",
    "stock" : 16.0,
    "price" : 20.0,
    "supplier_id" : "1",
    "onSale" : true,
    "origin" : [ 
        "China", 
        "U.S."
    ]
}

/* 2 */
{
    "_id" : ObjectId("62555a1c25b252e266ad2113"),
    "name" : "mango",
    "color" : "yellow",
    "stock" : 46.0,
    "price" : 10.0,
    "supplier_id" : "1",
    "onSale" : true,
    "origin" : [ 
        "Philippines", 
        "Thailand"
    ]
}

/* 3 */
{
    "_id" : ObjectId("62555a1c25b252e266ad2114"),
    "name" : "avocado",
    "color" : "green",
    "stock" : 57.0,
    "price" : 50.0,
    "supplier_id" : "2",
    "onSale" : true,
    "origin" : [ 
        "Mexico", 
        "Indonesia"
    ]
}

/* 4 */
{
    "_id" : ObjectId("62555a1c25b252e266ad2115"),
    "name" : "strawberry",
    "color" : "red",
    "stock" : 1000.0,
    "price" : 100.0,
    "supplier_id" : "2",
    "onSale" : true,
    "origin" : [ 
        "Philippines", 
        "Japan"
    ]
}
// ===================================
db.fruits.updateOne(
    {
        name: "strawberry"
    },
    {
        $set: {
            onSale : false
        }
    }
)


// **************************************
db.fruits.aggregate([
    {$match: { onSale:true}}
   ])

// ===================================

// ============ output =============
/* 1 */
{
    "_id" : ObjectId("62555a1c25b252e266ad2112"),
    "name" : "apple",
    "color" : "red",
    "stock" : 16.0,
    "price" : 20.0,
    "supplier_id" : "1",
    "onSale" : true,
    "origin" : [ 
        "China", 
        "U.S."
    ]
}

/* 2 */
{
    "_id" : ObjectId("62555a1c25b252e266ad2113"),
    "name" : "mango",
    "color" : "yellow",
    "stock" : 46.0,
    "price" : 10.0,
    "supplier_id" : "1",
    "onSale" : true,
    "origin" : [ 
        "Philippines", 
        "Thailand"
    ]
}

/* 3 */
{
    "_id" : ObjectId("62555a1c25b252e266ad2114"),
    "name" : "avocado",
    "color" : "green",
    "stock" : 57.0,
    "price" : 50.0,
    "supplier_id" : "2",
    "onSale" : true,
    "origin" : [ 
        "Mexico", 
        "Indonesia"
    ]
}
// ===================================
db.fruits.aggregate([
    {$match: { onSale:true}},
    {$group: {_id:"$supplier_id", total:{$sum:"$stock"}}}
   ])
//********** output ******************
/* 1 */
{
    "_id" : "1",
    "total" : 62.0
}

/* 2 */
{
    "_id" : "2",
    "total" : 57.0
}
// ===================================
//supplier_id  "1"    2.0

db.fruits.aggregate([
    {$match: { onSale:true}},
    {$group: {_id:"$supplier_id", total:{$sum:"$stock"}}},
    {$project: {_id: 0}}   // 1 or 0 for value; 1 shows id; 0 would show ttl only
   ])
//********** output ******************
/* 1 */
{
    "total" : 62.0
}

/* 2 */
{
    "total" : 57.0
}
// ===================================
db.fruits.aggregate([
    {$match: { onSale:true}},
    {$group: {_id:"$supplier_id", total:{$sum:"$stock"}}},
    {$project: {_id: 1, total: 0}}   // 1 or 0 for value; decides w/c field you want to show
   ])
//********** output ******************
/* 1 */
{
    "_id" : "1"
}

/* 2 */
{
    "_id" : "2"
}
// ===================================
db.fruits.aggregate([
    {$match: { onSale:true}},
    {$group: {_id:"$supplier_id", total:{$sum:"$stock"}}},
    {$sort: {total:1}} 
   ])
//********** output ******************
/* 1 */
{
    "_id" : "2",
    "total" : 57.0
}

/* 2 */
{
    "_id" : "1",
    "total" : 62.0
}
// ===================================
db.fruits.aggregate([
    {$match: { onSale:true}},
    {$group: {_id:"$supplier_id", total:{$sum:"$stock"}}},
    {$sort: {total:-1}} // arranges docs in a spec order; 1 smallest to largest;-1 the opppsite
   ])
//********** output ******************
/* 1 */
{
    "_id" : "1",
    "total" : 62.0
}

/* 2 */
{
    "_id" : "2",
    "total" : 57.0
}
//smaller value of ttl now in the bottom
// ===================================
db.fruits.aggregate([
{$unwind: "$origin"}  // deconstructs the array
])	
//********** output ******************
/* 1 */
{
    "_id" : ObjectId("62555a1c25b252e266ad2112"),
    "name" : "apple",
    "color" : "red",
    "stock" : 16.0,
    "price" : 20.0,
    "supplier_id" : "1",
    "onSale" : true,
    "origin" : "China"
}

/* 2 */
{
    "_id" : ObjectId("62555a1c25b252e266ad2112"),
    "name" : "apple",
    "color" : "red",
    "stock" : 16.0,
    "price" : 20.0,
    "supplier_id" : "1",
    "onSale" : true,
    "origin" : "U.S."
}

/* 3 */
{
    "_id" : ObjectId("62555a1c25b252e266ad2113"),
    "name" : "mango",
    "color" : "yellow",
    "stock" : 46.0,
    "price" : 10.0,
    "supplier_id" : "1",
    "onSale" : true,
    "origin" : "Philippines"
}

/* 4 */
{
    "_id" : ObjectId("62555a1c25b252e266ad2113"),
    "name" : "mango",
    "color" : "yellow",
    "stock" : 46.0,
    "price" : 10.0,
    "supplier_id" : "1",
    "onSale" : true,
    "origin" : "Thailand"
}

/* 5 */
{
    "_id" : ObjectId("62555a1c25b252e266ad2114"),
    "name" : "avocado",
    "color" : "green",
    "stock" : 57.0,
    "price" : 50.0,
    "supplier_id" : "2",
    "onSale" : true,
    "origin" : "Mexico"
}

/* 6 */
{
    "_id" : ObjectId("62555a1c25b252e266ad2114"),
    "name" : "avocado",
    "color" : "green",
    "stock" : 57.0,
    "price" : 50.0,
    "supplier_id" : "2",
    "onSale" : true,
    "origin" : "Indonesia"
}

/* 7 */
{
    "_id" : ObjectId("62555a1c25b252e266ad2115"),
    "name" : "strawberry",
    "color" : "red",
    "stock" : 1000.0,
    "price" : 100.0,
    "supplier_id" : "2",
    "onSale" : false,
    "origin" : "Philippines"
}

/* 8 */
{
    "_id" : ObjectId("62555a1c25b252e266ad2115"),
    "name" : "strawberry",
    "color" : "red",
    "stock" : 1000.0,
    "price" : 100.0,
    "supplier_id" : "2",
    "onSale" : false,
    "origin" : "Japan"
}
// ===================================
db.fruits.aggregate([
{$unwind: "$origin"}, 
{$group: {_id: "$origin", kinds: {$sum: 1}}} // sum refers to the no. of times the country appeared in the collection
])	
//********** output ******************
/* 1 */
{
    "_id" : "Indonesia",
    "kinds" : 1.0 // 1 kind of fruit being supplied by Indonesia
}

/* 2 */
{
    "_id" : "Japan",
    "kinds" : 1.0
}

/* 3 */
{
    "_id" : "U.S.",
    "kinds" : 1.0
}

/* 4 */
{
    "_id" : "China",
    "kinds" : 1.0
}

/* 5 */
{
    "_id" : "Mexico",
    "kinds" : 1.0
}

/* 6 */
{
    "_id" : "Philippines",
    "kinds" : 2.0
}

/* 7 */
{
    "_id" : "Thailand",
    "kinds" : 1.0
}
// ===================================
db.fruits.aggregate([
{$unwind: "$origin"}, 
{$match: {origin:"China"}},
{$group: {_id: "$origin", kinds: {$sum: 1}}}
])	
//********** output ******************
/* 1 */
{
    "_id" : "China",
    "kinds" : 1.0
}

//for a spec country only
// ===================================
var owner = ObjectId()
//********** output ******************
Script executed successfully but there are no results to show
// ===================================
//create owners collection w/ the ff fields:
// _id: owner,
// name:
// contact:

var owner = ObjectId()

db.owners.insert(
            {
                _id: owner,
                name: "Allan Mason",
                contact: "09112345678"
            }
    )
//********** output ******************
Inserted 1 record(s) in 473ms
// ===================================
db.getCollection('owners').find({})
//********** output ******************
/* 1 */
{
    "_id" : ObjectId("625574d525b252e266ad2121"),
    "name" : "Allan Mason",
    "contact" : "09112345678"
}
// ===================================
db.suppliers.insert({
	name:"ABC Fruits",
	contact: "09123456789",
	owner_id: ObjectId("625574d525b252e266ad2121") // insert owner's Objid#
})
//********** output ******************
Inserted 1 record(s) in 480ms
// ===================================
db.getCollection('suppliers').find({})
//********** output ******************
/* 1 */ 
{
    "_id" : ObjectId("6255762625b252e266ad2122"),   // abc fruits has this ID
    "name" : "ABC Fruits",
    "contact" : "09123456789",
    "owner_id" : ObjectId("625574d525b252e266ad2121") // abc fruits is owned by owner allan mason
}
// ===================================
db.suppliers.insert({
    name:"DEF Fruits",
    contact:"09987654321",
    address:[
        {street: "123 San Jose St.", city: "Manila"},
        {street: "367 Gil Puyat", city:"Makati"}
    ]
})
//********** output ******************
Inserted 1 record(s) in 468ms
// ===================================
db.getCollection('suppliers').find({}) 
//********** output ******************
/* 1 */    // 1 is to few relationship; rarely used
{
    "_id" : ObjectId("6255762625b252e266ad2122"),
    "name" : "ABC Fruits",
    "contact" : "09123456789",
    "owner_id" : ObjectId("625574d525b252e266ad2121")
}

/* 2 */
{
    "_id" : ObjectId("6255782725b252e266ad2123"),
    "name" : "DEF Fruits",
    "contact" : "09987654321",
    "address" : [    // array of objects
        {
            "street" : "123 San Jose St.",
            "city" : "Manila"
        }, 
        {
            "street" : "367 Gil Puyat",
            "city" : "Makati"
        }
    ]
}
// ===================================
var supplier = ObjectId()
var branch1 = ObjectId()
var branch2 = ObjectId()

db.suppliers.insert({
    _id: supplier,
    name:"GHI Fruits",
    contact:"09456789123",
    branches:[ branch1, branch2]
})
//********** output ******************
Inserted 1 record(s) in 461ms
// ===================================
db.getCollection('suppliers').find({}) 
//********** output ******************
/* 1 */
{
    "_id" : ObjectId("6255762625b252e266ad2122"),
    "name" : "ABC Fruits",
    "contact" : "09123456789",
    "owner_id" : ObjectId("625574d525b252e266ad2121")
}

/* 2 */
{
    "_id" : ObjectId("6255782725b252e266ad2123"),
    "name" : "DEF Fruits",
    "contact" : "09987654321",
    "address" : [ 
        {
            "street" : "123 San Jose St.",
            "city" : "Manila"
        }, 
        {
            "street" : "367 Gil Puyat",
            "city" : "Makati"
        }
    ]
}

/* 3 */
{
    "_id" : ObjectId("6255799b25b252e266ad2124"),
    "name" : "GHI Fruits",
    "contact" : "09456789123",
    "branches" : [ 
        ObjectId("6255799b25b252e266ad2125"), 
        ObjectId("6255799b25b252e266ad2126")
    ]
}
// ===================================

//********** output ******************

// ===================================

//********** output ******************

// ===================================

//********** output ******************