/* s25-activity.js */


// ==============================================================
//total number of fruits on sale: 4 fruits
db.fruits.aggregate( [
   { $count: "myCount" }
])
//*************************
/* 1 */
{
    "myCount" : 4
}
// *************************
// total number of fruits with stock more than 20

db.fruits.aggregate(
  [
    {
      $match: {
        stock: {
          $gt: 20
        }
      }
    },
    {
      $count: "stocks greater than 20"
    }
  ]
)
//*************************

/* 1 */
{
    "stocks greater than 20" : 3
}

//*************************
// average price of fruits onSale per supplier  //!! edit
db.fruits.aggregate(
   [
     {
       $match: 
         { onSale:true}},  
     {
       $group:
         {
           _id: "$supplier_id",
           avg_price: {$avg: "$price"}
         }
     }
   ]
)
//*************************
/* 1 */
{
    "_id" : "1",
    "avg_price" : 15.0
}

/* 2 */
{
    "_id" : "2",
    "avg_price" : 50.0
}
//*************************
//max operator to get the highest price of a fruit per supplier //!!

db.fruits.aggregate(
   [
     {
       $match: 
         { onSale:true}},  
     {
       $group:
         {
           _id: "$supplier_id",
           maxprice: {$max: "$price"}
         }
     }

   ]
)  
//*************************
/* 1 */
{
    "_id" : "1",
    "maxprice" : 20.0
}

/* 2 */
{
    "_id" : "2",
    "maxprice" : 50.0
}

//*************************
//lowest price of a fruit per supplier

db.fruits.aggregate(
   [
     {
       $match: 
         { onSale:true}},  
     {
       $group:
         {
           _id: "$supplier_id",
           min_price: {$min: "$price"}
         }
     }

   ]
)  
//*************************

/* 1 */
{
    "_id" : "1",
    "min_price" : 10.0
}

/* 2 */
{
    "_id" : "2",
    "min_price" : 50.0
}
//*************************


//*************************








